BlackJAX samplers
=================

.. currentmodule:: kernax

.. autosummary::
  :nosignatures:

  rmh
  hmc
  nuts
  mala

Random walk Metropolis Hastings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: kernax.rmh

Hamiltonian Monte Carlo
~~~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: kernax.hmc

NUTS Hamiltonian Monte Carlo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: kernax.nuts

Metropolis-Adjusted Langevin Algorithm
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: kernax.mala