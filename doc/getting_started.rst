Install guide
=============

Conda is the preferred way to install kernax.

Conda
~~~~~

A conda package is available on the conda-forge channel.

.. code::

    conda install -c conda-forge kernax

PyPI
~~~~

Alternatively, a pypi package is also available.

.. code::

    pip install kernax

From source
~~~~~~~~~~~

To install from source, clone this repository, then install it in editable mode:

.. code::

    pip install -e .