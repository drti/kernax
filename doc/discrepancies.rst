Kernel discrepancies
====================

.. currentmodule:: kernax

.. autosummary::
  :nosignatures:

  MMD
  KSD

Maximum Mean Discrepancy
~~~~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: kernax.MMD

Kernelized Stein Discrepancy
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: kernax.KSD