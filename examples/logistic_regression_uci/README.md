## Instructions

Note that before running this experiment, a few UCI datasets have to be downloaded.

1. Generate the MCMC samples by running

```console
bash run.sh
```

2. Compute the AUC metrics by running

```console
python compute_accuracy.py
```