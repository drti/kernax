## Instructions

1. Generate the MCMC samples by running

```console
bash run_mcmc.sh
```

2. Compute the MMD distances by running

```console
bash run_mmd.sh
```

3. Generate the figures by running

```console
python make_figures.py
```