Kernel functions
================

.. currentmodule:: kernax.kernels

.. autosummary::
  :nosignatures:

  IMQ
  Gaussian
  Energy
  SteinIMQ
  SteinGaussian
  GetSteinFn

Inverse multi-quadratric kernel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: kernax.kernels.IMQ

Gaussian kernel
~~~~~~~~~~~~~~~

.. autoclass:: kernax.kernels.Gaussian

Distance-based kernel
~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: kernax.kernels.Energy

Stein IMQ kernel
~~~~~~~~~~~~~~~~

.. autoclass:: kernax.kernels.SteinIMQ

Stein Gaussian kernel
~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: kernax.kernels.SteinGaussian

Stein kernel function
~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: kernax.kernels.GetSteinFn