Quantization algorithms
=======================

.. currentmodule:: kernax

.. autosummary::
  :nosignatures:

  KernelQuantization

MMD Quantization
~~~~~~~~~~~~~~~~

.. autoclass:: kernax.KernelQuantization
