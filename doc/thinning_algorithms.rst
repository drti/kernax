Thinning algorithms
===================

.. currentmodule:: kernax

.. autosummary::
  :nosignatures:

  SteinThinning
  RegularizedSteinThinning

Stein thinning
~~~~~~~~~~~~~~

.. autoclass:: kernax.SteinThinning

Regularized Stein thinning
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: kernax.RegularizedSteinThinning