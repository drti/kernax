Utilities
=========

.. currentmodule:: kernax.utils

.. autosummary::
  :nosignatures:

  median_heuristic
  laplace_log_p_softplus

Median heuristic
~~~~~~~~~~~~~~~~

.. autoclass:: kernax.utils.median_heuristic

Laplace regularization term
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: kernax.utils.laplace_log_p_softplus